Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: autodia
Source: http://www.aarontrevena.co.uk/opensource/autodia/

Files: *
Copyright: 2001-2009 Aaron Trevena <aaron.trevena@gmail.com>
           2007      Sean Dague
License: GPL-2
Comment: note from the upstream README file:
         In case of contribution, error or support, please, contact the
         upstream, via email, and put "[AutoDia]" in the subject line.

Files: templates/pod.tt
       lib/Autodia.pm
       lib/Autodia/Diagram.pm
       lib/Autodia/Diagram/Class.pm
       lib/Autodia/Handler/dia.pm
       lib/Autodia/Handler/Mason.pm
       lib/Autodia/Handler/Perl.pm
       lib/Autodia/Handler/umbrello.pm
Copyright: 2001-2007 Aaron Trevena <aaron.trevena@gmail.com>
           2007      A U Thor
License: Perl

Files: debian/*
Copyright: 2004      Cedric Gehin <cedric.gehin@epita.fr>
           2008-2011 Roland Stigge <stigge@antcom.de>
           2020      Fabio Dos Santos Mendes <fmendes@protonmail.ch>
License: GPL-2+

License: GPL-2 or GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: Perl
 It is free software; you can redistribute it and/or modify it under the
 terms of either:
 .
 a) the GNU General Public License as published by the Free Software
    Foundation; either version 1, or (at your option) any later
    version, or
 .
 b) the "Artistic License" which comes with Perl.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License version 1 can be found in `/usr/share/common-licenses/GPL-1'
 and the Artistic Licence in `/usr/share/common-licenses/Artistic'.
